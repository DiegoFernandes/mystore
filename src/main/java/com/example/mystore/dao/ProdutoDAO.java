package com.example.mystore.dao;

import com.example.mystore.entities.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ProdutoDAO extends JpaRepository<Produto, Long> {
}
