package com.example.mystore.controller;

import com.example.mystore.dao.ProdutoDAO;
import com.example.mystore.entities.Produto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/produto")
public class ProdutoViewController {

    @Autowired
    ProdutoDAO produtoDAO;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView homePage(){

        List<Produto> produtos = produtoDAO.findAll();

        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("produtos", produtos);

        return modelAndView;
    }

    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public ModelAndView datailPage(@PathVariable("id") Long id){

        Produto produto = produtoDAO.findOne(id);

        ModelAndView modelAndView = new ModelAndView("produtos/detail");
        modelAndView.addObject("produto", produto);

        return modelAndView;
    }

}
