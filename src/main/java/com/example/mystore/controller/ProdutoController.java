package com.example.mystore.controller;

import com.example.mystore.dao.ProdutoDAO;
import com.example.mystore.entities.Produto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/product")
public class ProdutoController {

    @Autowired
    ProdutoDAO produtoDAO;

    /*public ProdutoController(ProdutoDAO produtoDAO) {
        this.produtoDAO = produtoDAO;
    }*/

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<Produto>> list(){

        List<Produto> produtos = produtoDAO.findAll();

        if (CollectionUtils.isEmpty(produtos)){
            throw new IllegalArgumentException("Não foi buscar os produtos no banco de dados");
        }

        return ResponseEntity.ok().body(produtos);
    }

    @RequestMapping(value = "/by-id/{id}", method = RequestMethod.GET)
    public ResponseEntity<Produto> findById(@PathVariable("id")  Long id){

        Produto produto = produtoDAO.findOne(id);

        if (Objects.isNull(produto)){
            throw new IllegalArgumentException("Produto não encontrado para o Id: " + id);
        }

        return ResponseEntity.ok().body(produto);
    }

}
